# No Stranger SMS

Blocks messages from senders not in your contact list, filtering spam.

Warning: KitKat (Android 4.4) is not supported yet due to API change!

Features:

* silent notifications and storage of messages from unknown senders;
* add sender to system address book;
* delete message or move to Inbox;
* open source, under GPL license.

---
Блокировка сообщений от отправителей, отсутствующих в ваших контактах, отсеивающая спам.

Внимание: поддержка KitKat (Android 4.4) пока отсутствует из-за смены API!

Возможности:

* беззвучные уведомления и хранение сообщений от "незнакомцев";
* добавление отправителя в системную адресную книгу;
* удаление или отправка сообщения во входящие;
* открытый исходный код, лицензия GPL.

## Screenshot

[![t1]][1]

[t1]: http://glsk.net/wp-content/uploads/2013/08/nostrangersms_02-180x300.png
[1]: http://glsk.net/wp-content/uploads/2013/08/nostrangersms_02.png

## Download

Signed Android package: [nostrangersms.apk](http://glsk.net/playground/android/nostrangersms.apk) (Android 2.2+)

![QR code](http://glsk.net/playground/android/nostrangersms_qr.png "QR code")

Or get if from F-Droid repository:

[![t2]][2]

[t2]: http://glsk.net/wp-content/uploads/2013/08/get_it_on_f-droid_45.png
[2]: https://f-droid.org/repository/browse/?fdid=ru.glesik.nostrangersms

If you like this application and want to see it on Google Play, please [donate](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=JHX494AFDUU24) (Bitcoin: `1DbZjPqe4uaBv32deNqwbWUTrCempo2Wqk`) so I can pay registration fee.

## Changelog

1.4

 * Missing Android 4 icons added.

1.3

 * Date is now preserved when moving SMS to Inbox. Removed big borders around message list.

1.2

 * Notification priority set to LOW, Android 2 icons.

1.1

 * Option to delete all messages, refresh on notification click.

1.0

 * Initial release.

## License

Copyright (c) 2013 Alexander Inglessi (http://glsk.net).

Code licensed under [GNU GPL](http://www.gnu.org/licenses/gpl.html) version 3 or any later version. Artwork licensed under [CC BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/) 3 or any later version.
